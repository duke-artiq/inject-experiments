# Inject experiments

A collection of simple and universal inject experiments for various device classes.

Inject experiments in this repository are designed to be portable to any system running [ARTIQ](https://github.com/m-labs/artiq).

# Installation
 
The individual experiments in this repository can be copied to the `repository/` directory in an ARTIQ project and run as any experiment.
