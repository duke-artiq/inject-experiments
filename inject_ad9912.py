from artiq.experiment import *


class InjectAD9912(EnvExperiment):
    """Inject AD9912"""

    kernel_invariants = {
        'switch', 'freq', 'phase', 'set_att', 'att', 'init',
        'pre_delay', 'post_delay',
        'device',
    }

    def build(self):
        # Get devices
        self.setattr_device('core')
        self.device_dict = {k: self.get_device(k) for k, v in self.get_device_db().items()
                            if isinstance(v, dict) and v.get('class') == 'AD9912'}
        if not self.device_dict:
            self.device_dict['<No matching devices found>'] = None

        # Device arguments
        self.device_key = self.get_argument('Device',
                                            processor=EnumerationValue(sorted(self.device_dict)),
                                            tooltip='Choose a device')
        self.switch = self.get_argument('Switch',
                                        processor=BooleanValue(True),
                                        tooltip='The new switch state to set '
                                                '(when disabled, other parameters will not be injected)')
        self.freq = self.get_argument('Frequency',
                                      processor=NumberValue(200 * MHz, unit='MHz', min=0.0, max=400 * MHz),
                                      tooltip='Frequency')
        self.phase = self.get_argument('Phase',
                                       processor=NumberValue(0.0, min=0.0, max=1.0),
                                       tooltip='Phase in turns [0, 1)')
        self.set_att = self.get_argument('Set attenuation',
                                         processor=BooleanValue(False),
                                         tooltip='Load Urukul attenuation register and set attenuation '
                                                 '(adds additional required delays)')
        self.att = self.get_argument('Attenuation',
                                     processor=NumberValue(31.5 * dB, unit='dB', min=0 * dB, max=31.5 * dB),
                                     tooltip='Attenuation in dB (ignored if "Set attenuation" is False)')
        self.init = self.get_argument('Initialize',
                                      processor=BooleanValue(False),
                                      tooltip='Initialize Urukul and DDS before injection '
                                              '(adds additional required delays)')

        # Timing arguments
        self.pre_delay = self.get_argument('Pre-delay time',
                                           processor=NumberValue(5 * ms, unit='ms', step=1 * ms, min=0 * ms),
                                           group='Delays',
                                           tooltip='Wait time before configuring the device')
        self.post_delay = self.get_argument('Post-delay time',
                                            processor=NumberValue(0 * s, unit='s', step=1 * s, min=0 * s),
                                            group='Delays',
                                            tooltip='Wait time after the device is configured')

    def prepare(self):
        # Get the correct device
        self.device = self.device_dict[self.device_key]
        if self.device is None:
            raise RuntimeError(self.device_key)

        if self.set_att and self.att < 25 * dB:
            # Raise warning for low attenuation
            print('WARNING: attenuation < 25 dB can cause harmonics on your output signal')

    @kernel
    def run(self):
        # Reset the core
        self.core.reset()

        # Pre-delay
        delay(self.pre_delay)

        if self.init:
            # Initialize Urukul and DDS
            self.device.cpld.init()
            delay(2 * ms)
            self.device.init()
            self.core.break_realtime()
            delay(1 * ms)

        if self.set_att:
            # Set attenuation
            self.device.cpld.get_att_mu()
            delay(1 * ms)
            self.device.set_att(self.att)
            self.core.break_realtime()
            delay(1 * ms)

        if self.switch:
            # Only inject parameters if the switch is set to True
            self.device.set(self.freq, phase=self.phase)

        # Set switch
        self.device.sw.set_o(self.switch)

        # Post-wait time
        delay(self.post_delay)
        self.core.wait_until_mu(now_mu())
