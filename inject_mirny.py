from artiq.experiment import *


class InjectMirny(EnvExperiment):
    """Inject Mirny"""

    kernel_invariants = {
        'switch', 'freq', 'set_att', 'init', 'blind_init',
        'pre_delay', 'post_delay',
        'device', 'att_mu',
    }

    def build(self):
        self.setattr_device('core')
        self.device_dict = {k: self.get_device(k) for k, v in self.get_device_db().items()
                            if isinstance(v, dict) and v.get('class') == 'ADF5356'}
        if not self.device_dict:
            self.device_dict['<No matching devices found>'] = None

        # Device arguments
        self.device_key = self.get_argument('Device',
                                            processor=EnumerationValue(sorted(self.device_dict)),
                                            tooltip='Choose a device')
        self.switch = self.get_argument('Switch',
                                        processor=BooleanValue(True),
                                        tooltip='The new switch state to set '
                                                '(when disabled, other parameters will not be injected)')
        self.freq = self.get_argument('Frequency',
                                      processor=NumberValue(200 * MHz, unit='MHz', min=53.125 * MHz, max=6800 * MHz),
                                      tooltip='Frequency [53.125 MHz, 6.8 GHz]')
        self.set_att = self.get_argument('Set attenuation',
                                         processor=BooleanValue(False),
                                         tooltip='Set channel attenuation '
                                                 '(adds additional required delays)')
        self.att = self.get_argument('Attenuation',
                                     processor=NumberValue(0.0 * dB, unit='dB', min=0 * dB, max=31.5 * dB),
                                     tooltip='Attenuation in dB (ignored if "Set attenuation" is False)')
        self.init = self.get_argument('Initialize',
                                      processor=BooleanValue(False),
                                      tooltip='Initialize Mirny CPLD and channel before injection '
                                              '(adds additional required delays)')
        self.blind_init = self.get_argument('Blind initialization',
                                            processor=BooleanValue(False),
                                            tooltip='Whether to pass the "blind" flag to CPLD and channel init methods '
                                                    '(ignored if "Initialize" is False). See driver documentation'
                                                    'for details.')
        self.print_info = self.get_argument('Print device info',
                                            processor=BooleanValue(False),
                                            tooltip='If true, calls the driver\'s "info()" method after injection '
                                                    'and prints the result to the console. Useful for debugging.')

        # Timing arguments
        self.pre_delay = self.get_argument('Pre-delay time',
                                           processor=NumberValue(5 * ms, unit='ms', step=1 * ms, min=0 * ms),
                                           group='Delays',
                                           tooltip='Wait time before configuring the device')
        self.post_delay = self.get_argument('Post-delay time',
                                            processor=NumberValue(0 * s, unit='s', step=1 * s, min=0 * s),
                                            group='Delays',
                                            tooltip='Wait time after the device is configured')

    def prepare(self):
        # Get the correct device
        self.device = self.device_dict[self.device_key]
        if self.device is None:
            raise RuntimeError(self.device_key)
        # Convert attenuation to MU for ARTIQ 6 compatibility
        self.att_mu = 255 - int(round(self.att * 8))

    def run(self):
        self._run()
        if self.print_info:
            print(f'{self.device_key} info:')
            for k, v in self.device.info().items():
                print(f'\t{k}: {v}')

    @kernel
    def _run(self):
        # Reset the core
        self.core.reset()

        # Pre-delay
        delay(self.pre_delay)

        if self.init:
            # Initialize Mirny CPLD and channel
            self.device.cpld.init(blind=self.blind_init)
            delay(2 * ms)
            self.device.init(blind=self.blind_init)
            self.core.break_realtime()
            delay(1 * ms)

        if self.set_att:
            # Set attenuation
            self.device.set_att_mu(self.att_mu)
            self.core.break_realtime()
            delay(1 * ms)

        if self.switch:
            # Only inject parameters if the switch is set to True
            self.device.set_frequency(self.freq)

        # Set switch
        self.device.sw.set_o(self.switch)

        # Post-wait time
        delay(self.post_delay)
        self.core.wait_until_mu(now_mu())
