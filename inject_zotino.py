from artiq.experiment import *


class InjectZotino(EnvExperiment):
    """Inject Zotino"""

    kernel_invariants = {
        'voltages',
        'pre_delay', 'post_delay',
        'device',
        'user_led_hex',
    }

    def build(self):
        # Get devices
        self.setattr_device('core')
        self.device_dict = {k: self.get_device(k) for k, v in self.get_device_db().items()
                            if isinstance(v, dict) and v.get('class') == 'Zotino'}
        if not self.device_dict:
            self.device_dict['<No matching devices found>'] = None

        # Device arguments
        self.device_key = self.get_argument('Device',
                                            processor=EnumerationValue(sorted(self.device_dict)),
                                            tooltip='Choose a device')
        self.voltages = [self.get_argument(f'Output {i}',
                                           processor=NumberValue(0 * V, unit='V', ndecimals=4, min=-10, max=9.999),
                                           group=f'Output {(i // 8) * 8}-{((i // 8) * 8) + 7}',
                                           tooltip=f'Output voltage for output {i}')
                         for i in range(32)]

        # User LED arguments
        self.user_led = [self.get_argument(f'LED {i}',
                                           processor=BooleanValue(False),
                                           group='User LEDs',
                                           tooltip='Sets front panel LEDs')
                         for i in range(8)]

        self.init = self.get_argument('Initialize',
                                      processor=BooleanValue(False),
                                      tooltip='Initialize Zotino before injection '
                                              '(adds additional required delays)')

        # Timing arguments
        self.pre_delay = self.get_argument('Pre-delay time',
                                           processor=NumberValue(10 * ms, 'ms', step=1 * ms, min=0 * ms),
                                           group='Delays',
                                           tooltip='Wait time before configuring the device')
        self.post_delay = self.get_argument('Post-delay time',
                                            processor=NumberValue(0 * s, 's', step=1 * s, min=0 * s),
                                            group='Delays',
                                            tooltip='Wait time after the device is configured')

    def prepare(self):
        # Get the correct device
        self.device = self.device_dict[self.device_key]
        if self.device is None:
            raise RuntimeError(self.device_key)
        self.user_led.reverse()
        tmp = map(int, self.user_led)  # convert True to 1, False to 0  using `int`
        self.user_led_hex = int(''.join(map(str, tmp)), 2)  # numbers to strings, join them
        # convert to number (base 2)

    @kernel
    def run(self):
        # Reset the core
        self.core.reset()

        if self.init:
            # Initialize Zotino
            self.device.init()
            self.core.break_realtime()
            delay(1 * ms)

        # Pre-delay
        delay(self.pre_delay)

        # Inject parameters
        self.device.set_dac(self.voltages, channels=list(range(len(self.voltages))))
        self.device.set_leds(self.user_led_hex)

        # Post-wait time
        delay(self.post_delay)
        self.core.wait_until_mu(now_mu())
