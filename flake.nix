{
  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-unstable";
    flake8-artiq = {
      url = "git+https://gitlab.com/duke-artiq/flake8-artiq.git";
      inputs.nixpkgs.follows = "nixpkgs";
    };
  };

  outputs =
    {
      self,
      nixpkgs,
      flake8-artiq,
    }:
    let
      pkgs = nixpkgs.legacyPackages.x86_64-linux;

    in
    {
      devShells.x86_64-linux = {
        ci = pkgs.mkShell {
          packages = [
            (pkgs.python3.withPackages (ps: [
              flake8-artiq.packages.x86_64-linux.flake8-artiq
            ]))
          ];
        };
      };

      formatter.x86_64-linux = pkgs.nixfmt-rfc-style;
    };
}
